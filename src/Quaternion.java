import java.util.*;
import java.util.regex.*;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private final double real;
    private final double i;
    private final double j;
    private final double k;
    private static final double delta = 0.000001;


    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {

        this.real = a;
        this.i = b;
        this.j = c;
        this.k = d;

    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return this.real;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return this.i;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return this.j;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return this.k;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String str = String.valueOf(real);
        for (Double num : new Double[]{i, j, k}) {
            if (num.equals(0.)) {
                continue;
            }
            if (num > 0) {
                str += "+";
            }
            str += num;
            if (num.equals(i)) {
                str += "i";
            }
            if (num.equals(j)) {
                str += "j";
            }
            if (num.equals(k)) {
                str += "k";
            }

        }
        return str;
    }

    public static boolean correctComponents(String s) {
        char[] charArray = s.toCharArray();
        int iCount = 0;
        int jCount = 0;
        int kCount = 0;
        for (char each : charArray) {
            if (each == 'i') {
                iCount++;
            } else if (each == 'j') {
                jCount++;
            } else if (each == 'k') {
                kCount++;
            }
        }
        if (iCount > 1 || jCount > 1 || kCount > 1) {
            return false;
        }
        return true;
    }

    public static boolean correctOrder(String s) {
        List<Character> ijk = new LinkedList<>();
        for (char each : s.toCharArray()) {
            if (each == 'i' || each == 'j' || each == 'k') {
                ijk.add(each);
            }
        }
        for (int i = 1; i < ijk.size(); i++) {
            if (ijk.get(i - 1).compareTo(ijk.get(i)) > 0) {
                return false;
            }
        }
        return true;
    }


    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        if (!correctComponents(s) || !correctOrder(s)) {
            throw new RuntimeException("invalid string " + s);
        }
        String[] stringArray = new String[]{"0", "0i", "0j", "0k"};
        String numStr = String.valueOf(s.charAt(0));
        for (int i = 1; i <= s.length(); i++) {
            if (i == s.length() || s.charAt(i) == '+' || s.charAt(i) == '-') {

                if (numStr.contains("i")) {
                    stringArray[1] = numStr;
                } else if (numStr.contains("j")) {
                    stringArray[2] = numStr;
                } else if (numStr.contains("k")) {
                    stringArray[3] = numStr;
                } else {
                    stringArray[0] = numStr;
                }

                if (i < s.length()) {
                    numStr = String.valueOf(s.charAt(i));
                }
            } else {
                numStr += s.charAt(i);
            }
        }

        Double[] doubleArray = new Double[4];

        for (int i = 0; i < 4; i++) {
            if (stringArray[i].contains("i") || stringArray[i].contains("j") || stringArray[i].contains("k")) {
                stringArray[i] = stringArray[i].substring(0, stringArray[i].length() - 1);
            }
            try {
                Double num = Double.parseDouble(stringArray[i]);
                doubleArray[i] = num;
            } catch (NumberFormatException e) {
                throw new RuntimeException("invalid input " + s);
            }
        }
        return new Quaternion(doubleArray[0], doubleArray[1], doubleArray[2], doubleArray[3]);
    }
    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    /*public static Quaternion valueOf(String s) {
        //https://gist.github.com/mxrguspxrt/4044808
        Pattern p = Pattern.compile("(\\d\\.\\d)+(\\d\\.\\d)i+(\\d\\.\\d)j+(\\d\\.\\d)k");
        Matcher m = p.matcher(s);

        String[] realNumberAndRest = s.split("\\+", 2);
        double realNumber = Double.parseDouble(realNumberAndRest[0]);

        String[] iAndRest = realNumberAndRest[1].split("i\\+",3);
        double i = Double.parseDouble(iAndRest[0]);
        System.out.println(i);

        String[] jAndRest = iAndRest[1].split("j\\+", 3);
        double j = Double.parseDouble(jAndRest[0]);

        String[] kAndRest = jAndRest[1].split("k", 3);
        double k = Double.parseDouble(kAndRest[0]);
        System.out.println(new Quaternion(realNumber, i, j, k));
        return new Quaternion(realNumber, i, j, k);
    }*/


    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(real, i, j, k);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return equals(new Quaternion(0., 0., 0., 0.));
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(this.real, -this.i, -this.j, -this.k);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-real, -i, -j, -k);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(real + q.real, i + q.i, j + q.j, k + q.k);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double a1 = this.real;
        double b1 = this.i;
        double c1 = this.j;
        double d1 = this.k;

        double a2 = q.real;
        double b2 = q.i;
        double c2 = q.j;
        double d2 = q.k;

        double real2 = a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2;
        double i2 = a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2;
        double j2 = a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2;
        double k2 = a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2;

        return new Quaternion(real2, i2, j2, k2);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(this.real * r, this.i * r, this.j * r, this.k * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        double r = real * real + i * i + j * j + k * k;
        if (isZero()) {
            throw new RuntimeException("Division by zero is not allowed.");
        }
        return new Quaternion(
                real / r,
                (i * -1.) / r,
                (j * -1.) / r,
                (k * -1.) / r);
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return this.plus(q.opposite());
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Division by zero is not allowed.");
        }
        return times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Division by zero is not allowed.");
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if (qo instanceof Quaternion)
            return Math.abs(real - ((Quaternion) qo).real) < delta &&
                    Math.abs(i - ((Quaternion) qo).i) < delta &&
                    Math.abs(j - ((Quaternion) qo).j) < delta &&
                    Math.abs(k - ((Quaternion) qo).k) < delta;
        else
            return false;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        Quaternion m = times(q.conjugate()).plus(q.times(conjugate()));
        double r = Math.round(m.real / 2.);
        double i2 = Math.round(m.i / 2.);
        double j2 = Math.round(m.j / 2.);
        double k2 = Math.round(m.k / 2.);

        return new Quaternion(r, i2, j2, k2);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(real * real + i * i + j * j + k * k);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
        Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf(arg[0]);
        System.out.println("first: " + arv1.toString());
        System.out.println("real: " + arv1.getRpart());
        System.out.println("imagi: " + arv1.getIpart());
        System.out.println("imagj: " + arv1.getJpart());
        System.out.println("imagk: " + arv1.getKpart());
        System.out.println("isZero: " + arv1.isZero());
        System.out.println("conjugate: " + arv1.conjugate());
        System.out.println("opposite: " + arv1.opposite());
        System.out.println("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion) arv1.clone();
        } catch (CloneNotSupportedException e) {
        }
        ;
        System.out.println("clone equals to original: " + res.equals(arv1));
        System.out.println("clone is not the same object: " + (res != arv1));
        System.out.println("hashCode: " + res.hashCode());
        res = valueOf(arv1.toString());
        System.out.println("string conversion equals to original: "
                + res.equals(arv1));
        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf(arg[1]);
        System.out.println("second: " + arv2.toString());
        System.out.println("hashCode: " + arv2.hashCode());
        System.out.println("equals: " + arv1.equals(arv2));
        res = arv1.plus(arv2);
        System.out.println("plus: " + res);
        System.out.println("times: " + arv1.times(arv2));
        System.out.println("minus: " + arv1.minus(arv2));
        double mm = arv1.norm();
        System.out.println("norm: " + mm);
        System.out.println("inverse: " + arv1.inverse());
        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
        System.out.println("dotMult: " + arv1.dotMult(arv2));
    }
}
